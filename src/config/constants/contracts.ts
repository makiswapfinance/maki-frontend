export default {
  cake: {
    56: '0x45470a04a98881db0584F78B24b14acFBdDE3853',
    97: '0x45470a04a98881db0584F78B24b14acFBdDE3853',
  },
  masterChef: {
    56: '0x5fF09Bf3b7E07A8Fd14724e6aF86c10AE9a0C9d0',
    97: '',
  },
  wbnb: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '',
  },
  lottery: {
    56: '',
    97: '',
  },
  lotteryNFT: {
    56: '',
    97: '',
  },
  mulltiCall: {
    56: '0xdE4d38D15876a8887c12514558726e9eEEE17910',
    97: '0xdE4d38D15876a8887c12514558726e9eEEE17910',
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
  },
}
