import { useEffect } from 'react'
import { usePriceMakiBusd } from 'state/hooks'

const useGetDocumentTitlePrice = () => {
  const makiPriceUsd = usePriceMakiBusd()

  const makiPriceUsdString = makiPriceUsd.eq(0)
    ? ''
    : ` - $${makiPriceUsd.toNumber().toLocaleString(undefined, {
        minimumFractionDigits: 3,
        maximumFractionDigits: 3,
      })}`

  useEffect(() => {
    document.title = `MAKISWAP.FINANCE${makiPriceUsdString}`
  }, [makiPriceUsdString])
}
export default useGetDocumentTitlePrice
