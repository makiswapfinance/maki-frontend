import React from 'react'
import { Card, CardBody, Heading, Text } from '@makiswap/uikit'
import BigNumber from 'bignumber.js/bignumber'
import styled from 'styled-components'
import { getBalanceNumber } from 'utils/formatBalance'
import { useTotalSupply, useBurnedBalance } from 'hooks/useTokenBalance'
import useI18n from 'hooks/useI18n'
import { getCakeAddress } from 'utils/addressHelpers'
import CardValue from './CardValue'
import { useFarms, usePriceMakiBusd } from '../../../state/hooks'

const StyledMakiStats = styled(Card)`
  margin-left: auto;
  margin-right: auto;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  margin-bottom: 8px;
`

const MakiStats = () => {
  const TranslateString = useI18n()
  const totalSupply = useTotalSupply()
  const burnedBalance = useBurnedBalance(getCakeAddress())
  const farms = useFarms()
  const makiPrice = usePriceMakiBusd()
  const circSupply = totalSupply ? totalSupply.minus(burnedBalance) : new BigNumber(0)
  const makiSupply = getBalanceNumber(circSupply)
  const marketCap = makiPrice.times(circSupply)

  let makiPerBlock = 0
  if (farms && farms[0] && farms[0].makiPerBlock) {
    makiPerBlock = new BigNumber(farms[0].makiPerBlock).div(new BigNumber(10).pow(18)).toNumber()
  }

  return (
    <StyledMakiStats>
      <CardBody>
        <Heading size="xl" mb="24px">
          {TranslateString(534, 'MAKI Stats')}
        </Heading>
        <Row>
          <Text fontSize="14px">{TranslateString(536, 'Total MAKI Supply')}</Text>
          {makiSupply && <CardValue fontSize="14px" value={makiSupply} decimals={0} />}
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(999, 'Market Cap')}</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(marketCap)} decimals={0} prefix="$" />
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(538, 'Total MAKI Burned')}</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(burnedBalance)} decimals={0} />
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(540, 'New MAKI/block')}</Text>
          <Text bold fontSize="14px">
            {makiPerBlock}
          </Text>
        </Row>
      </CardBody>
    </StyledMakiStats>
  )
}

export default MakiStats
