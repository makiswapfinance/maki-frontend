import { MenuEntry } from '@makiswap/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x45470a04a98881db0584F78B24b14acFBdDE3853',
        external: true,
      },
      {
        label: 'Liquidity',
        href: 'https://exchange.pancakeswap.finance/#/add/BNB/0x45470a04a98881db0584F78B24b14acFBdDE3853',
        external: true,
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Bar',
    icon: 'BarIcon',
    href: '/bar',
  },
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'PancakeSwap',
        href: 'https://pancakeswap.info/token/0x45470a04a98881db0584F78B24b14acFBdDE3853',
        external: true,
      },
    ],
  },
  {
    label: 'Blog',
    icon: 'MediumIcon',
    href: 'https://makiswapfinance.medium.com',
    external: true,
  },
]

export default config
